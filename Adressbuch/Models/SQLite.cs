﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Windows.Storage;

namespace Adressbuch.Models
{
    public class SQLite
    {
        public async static void InitializeDatabase()
        {
            await ApplicationData.Current.LocalFolder.CreateFileAsync("addresses.db", CreationCollisionOption.OpenIfExists);
            string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "addresses.db");

            using (SqliteConnection db = new SqliteConnection($"Filename={dbpath}"))
            {
                db.Open();

                string tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS addresses (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "firstName NVARCHAR(2048) NULL, " +
                    "lastName NVARCHAR(2048) NULL, " +
                    "street NVARCHAR(2048) NULL, " +
                    "houseNumber NVARCHAR(2048) NULL, " +
                    "postalCode NVARCHAR(2048) NULL, " +
                    "city NVARCHAR(2048) NULL, " +
                    "email NVARCHAR(2048) NULL, " +
                    "phoneNumber NVARCHAR(2048) NULL, " +
                    "landline NVARCHAR(2048) NULL, " +
                    "notes NVARCHAR(2048) NULL, " +
                    "_group NVARCHAR(2048) NULL)";

                SqliteCommand createTable = new SqliteCommand(tableCommand, db);

                createTable.ExecuteReader();
            }
        }

        public static void Insert(string firstName, string lastName, string street, string houseNumber, string postalCode, string city, string email, string phoneNumber, string landline, string notes, string group)
        {
            string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "addresses.db");

            using (SqliteConnection db = new SqliteConnection($"Filename={dbpath}"))
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                // Use parameterized query to prevent SQL injection attacks
                insertCommand.CommandText = "INSERT INTO addresses (firstName, lastName, street, houseNumber, postalCode, city, email, phoneNumber, landline, notes, _group) " +
                                            "VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11);";
                insertCommand.Parameters.AddWithValue("@param1", firstName.Trim());
                insertCommand.Parameters.AddWithValue("@param2", lastName.Trim());
                insertCommand.Parameters.AddWithValue("@param3", street.Trim());
                insertCommand.Parameters.AddWithValue("@param4", houseNumber.Trim());
                insertCommand.Parameters.AddWithValue("@param5", postalCode.Trim());
                insertCommand.Parameters.AddWithValue("@param6", city.Trim());
                insertCommand.Parameters.AddWithValue("@param7", email.Trim());
                insertCommand.Parameters.AddWithValue("@param8", phoneNumber.Trim());
                insertCommand.Parameters.AddWithValue("@param9", landline.Trim());
                insertCommand.Parameters.AddWithValue("@param10", notes.Trim());
                insertCommand.Parameters.AddWithValue("@param11", group.Trim());

                insertCommand.ExecuteReader();

                db.Close();
            }

        }

        public static List<Address> Select()
        {
            List<Address> entries = new List<Address>();

            string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "addresses.db");

            using (SqliteConnection db = new SqliteConnection($"Filename={dbpath}"))
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand
                    ("SELECT * from addresses ORDER BY firstName COLLATE NOCASE ASC", db);

                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    Address address = new Address
                    {
                        Id = (long)query[0],
                        FirstName = (string)query[1],
                        LastName = (string)query[2],
                        FullName = (string)query[1] + " " + (string)query[2],
                        Street = (string)query[3],
                        HouseNumber = (string)query[4],
                        PostalCode = (string)query[5],
                        City = (string)query[6],
                        Email = (string)query[7],
                        Phone = (string)query[8],
                        Landline = (string)query[9],
                        Notes = (string)query[10],
                        Group = (string)query[11]
                    };

                    entries.Add(address);
                }

                db.Close();
            }

            return entries;
        }

        public static void Delete(int id)
        {
            string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "addresses.db");

            using (SqliteConnection db = new SqliteConnection($"Filename={dbpath}"))
            {
                db.Open();

                SqliteCommand deleteCommand = new SqliteCommand();
                deleteCommand.Connection = db;

                deleteCommand.CommandText = "DELETE from addresses WHERE id = @param1;";
                deleteCommand.Parameters.AddWithValue("@param1", id);

                deleteCommand.ExecuteReader();

                db.Close();
            }
        }

        public static void Update(int id, string firstName, string lastName, string street, string houseNumber, string postalCode, string city, string email, string phoneNumber, string landline, string notes, string group)
        {
            string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "addresses.db");

            using (SqliteConnection db = new SqliteConnection($"Filename={dbpath}"))
            {
                db.Open();

                SqliteCommand updateCommand = new SqliteCommand();
                updateCommand.Connection = db;

                updateCommand.CommandText = "UPDATE addresses SET firstName = @param1, lastName = @param2, street = @param3, houseNumber = @param4, postalCode = @param5, " +
                                            "city = @param6, email = @param7, phoneNumber = @param8, landline = @param9, notes = @param10, _group = @param11 WHERE id = @param12;";
                updateCommand.Parameters.AddWithValue("@param1", firstName.Trim());
                updateCommand.Parameters.AddWithValue("@param2", lastName.Trim());
                updateCommand.Parameters.AddWithValue("@param3", street.Trim());
                updateCommand.Parameters.AddWithValue("@param4", houseNumber.Trim());
                updateCommand.Parameters.AddWithValue("@param5", postalCode.Trim());
                updateCommand.Parameters.AddWithValue("@param6", city.Trim());
                updateCommand.Parameters.AddWithValue("@param7", email.Trim());
                updateCommand.Parameters.AddWithValue("@param8", phoneNumber.Trim());
                updateCommand.Parameters.AddWithValue("@param9", landline.Trim());
                updateCommand.Parameters.AddWithValue("@param10", notes.Trim());
                updateCommand.Parameters.AddWithValue("@param11", group.Trim());
                updateCommand.Parameters.AddWithValue("@param12", id);

                updateCommand.ExecuteReader();

                db.Close();
            }
        }
    }
}
