﻿using Adressbuch.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.ViewManagement;

namespace Adressbuch.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public SQLite SQLite = new SQLite();

        // Notifies the View about changes
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Checks if the two parameters are equal and notifys the View if they're not        
        public void SetValue<T>(ref T Backingfield, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(Backingfield, value))
            {
                return;
            }
            else
            {
                Backingfield = value;

                this.OnPropertyChanged(propertyName);
            }
        }
    }
}
