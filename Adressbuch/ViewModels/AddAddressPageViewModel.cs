﻿using Adressbuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Adressbuch.ViewModels
{
    public class AddAddressPageViewModel : BaseViewModel
    {
        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Street { get; set; } = string.Empty;

        public string HouseNumber { get; set; } = string.Empty;

        public string PostalCode { get; set; } = string.Empty;

        public string City { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string PhoneNumber { get; set; } = string.Empty;

        public string Landline { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;

        public string SelectedGroup { get; set; } = string.Empty;

        public List<string> Groups = new List<string>
        {
            "Arbeitskollegen",
            "Familie",
            "Freunde"
        };

        public ICommand AddAddressCommand
        {
            get
            {
                return new CommandHandler(() => this.AddAddress());
            }
        }

        private void AddAddress()
        {
            SQLite.Insert(this.FirstName, this.LastName, this.Street, this.HouseNumber, this.PostalCode, this.City, this.Email, this.PhoneNumber, this.Landline, this.Notes, this.SelectedGroup);

            Window.Current.Close();
        }
    }
}
