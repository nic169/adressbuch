﻿using Adressbuch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace Adressbuch.ViewModels
{
    public class ViewAddressPageViewModel : BaseViewModel
    {
        private int id;
        
        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Street { get; set; } = string.Empty;

        public string HouseNumber { get; set; } = string.Empty;

        public string PostalCode { get; set; } = string.Empty;

        public string City { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public string PhoneNumber { get; set; } = string.Empty;

        public string Landline { get; set; } = string.Empty;

        public string Notes { get; set; } = string.Empty;

        public string Group { get; set; } = string.Empty;

        private bool readOnly;

        public bool ReadOnly
        {
            get
            {
                return this.readOnly;
            }
            set
            {
                this.SetValue(ref this.readOnly, value);
            }
        }

        private bool btnOneEnabled;

        public bool BtnOneEnabled
        {
            get
            {
                return this.btnOneEnabled;
            }
            set
            {
                this.SetValue(ref this.btnOneEnabled, value);
            }
        }

        private bool btnTwoEnabled;

        public bool BtnTwoEnabled
        {
            get
            {
                return this.btnTwoEnabled;
            }
            set
            {
                this.SetValue(ref this.btnTwoEnabled, value);
            }
        }

        public ICommand EditAddressCommand
        {
            get
            {
                return new CommandHandler(() => this.EnableEdit());
            }
        }

        public ICommand SaveChangesCommand
        {
            get
            {
                return new CommandHandler(() => this.SaveChanges());
            }
        }

        public ICommand DeleteAddressCommand
        {
            get
            {
                return new CommandHandler(() => this.DeleteAddress());
            }
        }

        public List<string> Groups = new List<string>
        {
            "Arbeitskollegen",
            "Familie",
            "Freunde"
        };       

        public ViewAddressPageViewModel(Address address)
        {
            this.ReadOnly = true;
            this.BtnOneEnabled = true;
            
            if (address != null)
            {
                this.id = (int)address.Id;
                this.FirstName = address.FirstName;
                this.LastName = address.LastName;
                this.Street = address.Street;
                this.HouseNumber = address.HouseNumber;
                this.PostalCode = address.PostalCode;
                this.City = address.City;
                this.Email = address.Email;
                this.PhoneNumber = address.Phone;
                this.Landline = address.Landline;
                this.Notes = address.Notes;
                this.Group = address.Group;
            }
        }

        private void EnableEdit()
        {
            this.ReadOnly = false;
            this.BtnOneEnabled = false;
            this.BtnTwoEnabled = true;
        }

        private void SaveChanges()
        {
            this.ReadOnly = true;
            this.BtnOneEnabled = true;
            this.BtnTwoEnabled = false;
            
            SQLite.Update(this.id, this.FirstName, this.LastName, this.Street, this.HouseNumber, this.PostalCode, this.City, this.Email, this.PhoneNumber, this.Landline, this.Notes, this.Group);
        }

        private async void DeleteAddress()
        {
            var dialog = new MessageDialog("Willst du diese Adresse wirklich löschen?");
            dialog.Commands.Add(new UICommand { Label = "Abbrechen", Id = 1 });
            dialog.Commands.Add(new UICommand { Label = "Löschen", Id = 2 });

            var result = await dialog.ShowAsync();

            if ((int)result.Id == 2)
            {
                SQLite.Delete(this.id);
                Window.Current.Close();
            }
        }
    }
}
