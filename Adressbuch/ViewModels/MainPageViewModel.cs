﻿using Adressbuch.Models;
using Adressbuch.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Adressbuch.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        public ICommand ToAddAddressCommand
        {
            get
            {
                return new CommandHandler(() => this.Navigate("add"));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new CommandHandler(() => this.Addresses = SQLite.Select());
            }
        }

        private List<Address> addresses;

        public List<Address> Addresses
        {
            get
            {
                return this.addresses;
            }
            set
            {
                this.SetValue(ref this.addresses, value);
            }
        }

        private Address selectedItem;

        public Address SelectedItem
        {
            get
            {
                return this.selectedItem;
            }
            set
            {
                this.SetValue(ref this.selectedItem, value);

                if (this.selectedItem != null)
                {
                    this.Navigate("view");
                }
            }
        }

        private string searchQuery;

        public string SearchQuery
        {
            get
            {
                return this.searchQuery;
            }
            set
            {
                this.SetValue(ref this.searchQuery, value);
                this.SearchAddress();
            }
        }


        public MainPageViewModel()
        {
            this.Addresses = SQLite.Select();
        }

        private async void Navigate(string page)
        {
            CoreApplicationView newView = CoreApplication.CreateNewView();
            var newViewId = 0;

            await newView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Frame frame = new Frame();

                if (page == "add")
                {
                    frame.Navigate(typeof(AddAddressPage), this);
                }
                else if (page == "view")
                {
                    frame.Navigate(typeof(ViewAddressPage), this.SelectedItem);
                }

                Window.Current.Content = frame;

                // You have to activate the window in order to show it later.
                Window.Current.Activate();
                Window.Current.Closed += Current_Closed;
                newViewId = ApplicationView.GetForCurrentView().Id;
            });

            bool viewShown = await ApplicationViewSwitcher.TryShowAsStandaloneAsync(newViewId);
        }

        private async void Current_Closed(object sender, CoreWindowEventArgs e)
        {
            await CoreApplication.MainView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.Addresses = SQLite.Select();
            });
        }
        
        private void SearchAddress()
        {
            this.Addresses = SQLite.Select();
            List<Address> addresses = new List<Address>();
            
            foreach (var address in this.Addresses)
            {
                if (address.FullName.ToLower().Contains(this.SearchQuery.ToLower()) || address.Group.ToLower().Contains(this.SearchQuery.ToLower()))
                {
                    addresses.Add(address);
                }
            }

            this.Addresses = addresses;
        }
    }
}
