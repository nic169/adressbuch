﻿using Adressbuch.Models;
using Adressbuch.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Adressbuch.Views
{
    public sealed partial class ViewAddressPage : Page
    {
        private ViewAddressPageViewModel viewModel;
        private Address address;

        public ViewAddressPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.address = (Address)e.Parameter;
            this.viewModel = new ViewAddressPageViewModel(this.address);
            this.DataContext = viewModel;            
        }
    }
}
